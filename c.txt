La scène professionnelle que je vais décrire s’est déroulé il y a un peu plus d’un mois, lorsque je travaillais dans une colonie de vacances en Suisse.
Je n’avais jamais travaillé dans un autre pays que la France.
Le directeur de la colonie classait ses onze animateurs par ordre de préférence (comment ?) et invitait implicitement (pas factuel) les enfants à faire de même grâce à un tableau où ils pouvaient écrire des petits mots à leurs animateurs favoris.
Je n’ai jamais eu de petits mots.
Il n’y avait pas véritablement de règle même en temps de crise sanitaire.
A la fin de ces deux semaines, il était convenu que nous (les animateurs) nous réunissions pour rédiger ce qu’ils appellent les « diplômes ».
On attribuait le diplôme « du plus gentil », « du plus frimeur », ou encore « de l’insomniaque », mais tous les enfants avaient un diplôme type avec un mot de chaque animateur.
Ce système de récompense par le diplôme, véritable prolongement du système scolaire, mais cette fois-ci non pas sur les capacités intellectuelles de l’enfant, mais sur son comportement social.
Le lendemain, avant de rendre les enfants à leurs parents, on les avait rassemblés pour faire le bilan de leur semaine.
Chaque animateur choisissait une liasse de diplômes qu’il allait ensuite distribuer un par un, en lâchant un petit mot théâtralisé devant tout le monde « J’ai un diplôme pour un enfant avec qui j’ai passé des supers moments, toujours rieur, toujours motivé pour faire les activités, un tonnerre d’applaudissement pour Léo ! ».
J’avais été la première à choisir les enfants à qui j’avais envie de remettre un diplôme, j’en avais pris le moins et j’avais fait en sorte que l’enfant parle français.
Après cela, c’était au tour des enfants de s’exprimer sur leur semaine.
A chaque fois que l’un d’entre eux disait qu’il avait passé une super semaine car les animateurs étaient trop cools.
Arriva le tour d’un enfant avec qui je n’avais pas eu de contact durant la semaine, il se leva avec la raquette de ping-pong qui lui servait de micro et commença d’une voie assurer à annoncer qu’il allait établir un classement des meilleurs moniteurs en commençant par la fin.
Là mon nom retenti en premier, les enfants se mirent à rire et je me figeai avec un sourire forcé.
Je ris en lui disant que ce n’était pas grave, faisant mine que ça ne m’atteignait pas.
Les autres animatrices m’adressaient un regard compatissant, alors que les animateurs restaient de marbre attendant leur tour.
Par la suite, le directeur a essayé de me rassurer en me disant que chaque animateur était différent et que moi j’étais très compétente dans tel ou tel moment d’animation.
J’étais en colère contre les animateurs (un animateur en particulier qui était le « chef »), qui s’étaient imposés comme des figures d’autorité, de ne pas avoir arrêté cette mascarade, d’avoir laissé l’enfant réduire d’autre individu à des performances sociales, cela se traduisait par des regards noirs à leurs encontre.
J’étais en colère contre moi-même de me sentir aussi touché par l’avis de l’enfant.
Je rougissais de honte à l’annonce de mon prénom dans la bouche de l’enfant. 
Je ressentis le besoin de m’isoler afin de pouvoir évacuer ma colère qui se transforma par la suite en tristesse.
Mes émotions se mélangeaient mais plus ma valise se remplissait [sur le départ] plus la joie prenait le dessus.
Je m’imaginais que les enfants ne m’appréciaient pas et que je n’avais rien à faire ici.
Je ne me jugeais « pas-fun », « pas-drôle », pas assez ceci ou pas assez cela Je me compare aux autres animateurs.
Le fonctionnement de cette colonie de vacances allait à l’encontre de mes opinions en terme de pédagogie.
Je ne voulais plus jamais revenir dans cet endroit. 
J’ai pensé que l’animateur référent, qui n’avait pas arrêté la mascarade, avait envie de savoir qui était dans le top trois des meilleurs animateurs selon cet adolescent. 
Je pensais que tout le monde savait que j’étais l’animatrice la moins compétente et que tout le monde voulais que ce soit dit haut et fort.
Je suis en train de vivre en premier lieu de la peur, la peur d’être « démasqué » aux yeux de tous comme une mauvaise animatrice.
Ensuite de la colère qui précède à la tristesse.
Il se traduisent physiquement (comment ?) par de la honte (ce n’est pas un ressenti physique).
En effet j’essaie de montrer que cette annonce ne m’atteint pas en cachant mon envie de pleurer et en cachant mes tremblements.
A la fin de cette scène quand je me retrouve seule dans ma chambre, à faire ma valise, je ressens de la joie à l’idée de ne plus avoir d’enfants à ma charge et de ne plus avoir de comptes à rendre aux autres adultes de la colonie de vacances.
Je suis heureuse à l’idée de ne plus jamais revoir ces individus qui ne m’ont rien appris.
