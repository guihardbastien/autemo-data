J'étais dans ma classe préparatoire, au lycée dans ma salle de classe, à côté de mes amis, en cours d'anglais, dans le contexte de la crise sanitaire et à un mois du concours de l'ENS.
Notre professeur d'anglais nous a annoncé la nouvelle, que nous allions devoir nous préparer au concours via visioconférence, mes yeux se sont écarquillés, je me suis tournée vers mon voisin pour lui communiquer mes inquiétudes tout en me rongeant les ongles et en respirant fort.
Mon voisin aussi semblait choqué.
Ce que je ressens : J'ai ressenti comme des petites épingles dans mes bras, j'ai eu un peu mal au ventre, ma gorge s'est serrée, et j'ai eu un peu mal à la tête , une sorte de vertige à cause des différentes sensations que j'ai ressenti.
Mais quand j'en ai parlé avec mon voisin ma posture est devenue moins raide et je me suis un peu relaxée .
Ce que je pense : Au début de l'annonce je me suis dit que j'allais pouvoir faire des grasse matinées et ne plus me lever aussi tôt.
Mais ensuite j'ai pensé 'oh mon dieu qu'est ce que je vais faire de mes journées' je me suis vue ne rien faire tout en voyant le concours arriver, je me disais que j'allais me laisser aller et ne rien faire et qu'une fois au concours, j'allais complètement rater et que je 'allais plus voir mes amis, perdre une certaine routine, ce qui m'a rendu triste.
Mon territoire attaqué est celui de mes habitudes, mon école, ma routine qui s'était installée, je pensais que j'allais être accompagnée jusqu'au concours par mes professeurs, mais tout cela a été ruiné.
